export interface Variant {
    id: number;
    categoryId: number;
    variantCode: string;
    variantName: string;
}