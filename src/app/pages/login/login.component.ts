import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: User = new User();
 

  constructor(private authService : AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  register() {
    this.router.navigate(['register']);
  }

  login() {
    this.authService.login(this.user).subscribe(
      (data:any) => {
        console.log(data);
        this.router.navigate(['home']);
      },
      (err) => {
        console.log(err.error);
      },
      () => console.log('Login Successfully')
    )
    
  }

  

}
