import { HttpClient, HttpHeaders } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Config } from '../base/config';
import { Category } from '../model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json',
      'Access-Control-Allow-Origin' : '*',
      Authorization: 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJuaXRhIiwiaWF0IjoxNjI1NTUzNDg2fQ.R3cCPJEWXeQdAe4Yskm6AUe33mtNyvBxhzSnlM8Lmlk6Lg4noAIMqRePgeQm1n6KjaQq6_JV7misPF2qoq3VBg'
    })
  }

  public getCategory(): Observable<Category[]> {
    return this.http.get<Category[]> (
      `${Config.url}api/category`,
      this.httpOptions
    )
  }

  public addCategory(category: Category): Observable<Category> {
      return this.http.post<Category>(
      Config.url + 'api/category',
      category, {
        ...this.httpOptions,
        responseType: 'text' as 'json',
      }
    )
  }

  public editCategory(category: Category): Observable<Category> {
    return this.http.put<Category>(
    Config.url + 'api/category',
    category, {
      ...this.httpOptions,
      responseType: 'text' as 'json',
    }
  )
  }

  public deleteCategory(id: number): Observable<void> {
    return this.http.delete<void>(
    Config.url + `api/category/${id}`, {
      ...this.httpOptions,
      responseType: 'text' as 'json',
    }
  )
  }
}
